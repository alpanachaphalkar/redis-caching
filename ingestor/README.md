# Ingestor Service
- It fetches the taxis and their positions regularly and feeds them into a Caching System.
- And it invalidates outdated data, e.g. when a taxi becomes unavailable or moved.

[Here](Dockerfile) is the Dockerfile to build the docker image.

To run this service locally, use below command:
```s
docker run -e POSITIONS_HOST=positions -e POSITIONS_PORT=8080 -e \
 -e REDIS_HOST=redis -e REDIS_PORT=6379 \
 alpanachaphalkar/ingestor:latest
```