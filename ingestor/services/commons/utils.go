package commons

import (
	"os"
)

func GetEnv(key, fallback string)  string{
	if value, ok := os.LookupEnv(key); ok {
		if len(value) == 0 {
			return fallback
		}
		return value
	}
	return fallback
}
