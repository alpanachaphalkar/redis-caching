package services

import (
	"context"
	"encoding/json"
	"time"

	redis "github.com/go-redis/redis/v8"
	log "github.com/sirupsen/logrus"
	utils "github.com/xPlorinRolyPoly/redis-caching/ingestor/services/commons"
)

var ctx = context.Background()

const CacheNotFound = redis.Nil

func con() *redis.Client {
	var redisHost string = utils.GetEnv("REDIS_HOST", "localhost")
	var redisPort string = utils.GetEnv("REDIS_PORT", "6379")
	var redisUsername string = utils.GetEnv("REDIS_USERNAME", "")
	var redisPassword string = utils.GetEnv("REDIS_PASSWORD", "")
	rdb := redis.NewClient(&redis.Options{
		Addr:     redisHost + ":" + redisPort,
		Username: redisUsername,
		Password: redisPassword, // no password set
		DB:       0,             // use default DB
	})
	return rdb
}

func (p position) Cache() {
	client := con()
	defer client.Close()
	jsonValue, er := json.Marshal(p)
	if er != nil {
		log.Error(er)
		panic(er)
	}
	e := client.SetEX(ctx, string(p.ID), jsonValue, 3*time.Minute).Err()
	if e != nil {
		log.Error(e)
		panic(e)
	}
	log.Info("" + p.ID + " is cached into Redis DB")
}

func GetCache(key string) (position, error) {
	client := con()
	defer client.Close()
	var p position
	v, e := client.Get(ctx, key).Result()
	if e != nil {
		return p, e
	}
	json.Unmarshal([]byte(v), &p)
	return p, nil
}

func (p positions) LoadCache() {
	for _, pos := range p {
		pos.Cache()
	}
	log.Info("Redis Cache is Loaded.")
}

func InvalidateCache(key string) {
	client := con()
	defer client.Close()
	client.Del(ctx, key)
	log.Info("" + key + " is invalidated into Cache.")
}
