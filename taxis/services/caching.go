package services

import (
	"context"
	"encoding/json"

	redis "github.com/go-redis/redis/v8"
	utils "github.com/xPlorinRolyPoly/redis-caching/taxis/services/commons"
)

var ctx = context.Background()

type Position struct {
	ID        string `json:"ID"`
	PosX      int    `json:"PosX"`
	PosY      int    `json:"PosY"`
	Available bool   `json:"Available"`
}

func con() *redis.Client {
	var redisHost string = utils.GetEnv("REDIS_HOST", "localhost")
	var redisPort string = utils.GetEnv("REDIS_PORT", "6379")
	var redisUsername string = utils.GetEnv("REDIS_USERNAME", "")
	var redisPassword string = utils.GetEnv("REDIS_PASSWORD", "")
	rdb := redis.NewClient(&redis.Options{
		Addr:     redisHost + ":" + redisPort,
		Username: redisUsername,
		Password: redisPassword, // no password set
		DB:       0,             // use default DB
	})
	return rdb
}

func GetCache() (ps []Position) {
	client := con()
	defer client.Close()
	vs, er := client.Keys(ctx, "*").Result()
	if er != nil {
		panic(er)
	}
OUTER:
	for _, k := range vs {
		v, er := client.Get(ctx, k).Result()
		if er != nil {
			if er == redis.Nil {
				continue OUTER
			}
			panic(er)
		}
		var p Position
		json.Unmarshal([]byte(v), &p)
		if bool(p.Available) {
			ps = append(ps, p)
		}
	}

	return
}
