package main

import (
	log "github.com/sirupsen/logrus"
	service "github.com/xPlorinRolyPoly/redis-caching/ingestor/services"
)

func main() {
	log.Info("Ingestor Service")
	positions := service.GetPositions()
	positions.LoadCache()

	for {
		for _, pos := range positions {
			v, er := service.GetCache(pos.ID)
			if er == service.CacheNotFound {
				p, e := service.GetPosition(pos.ID)
				if e != nil {
					log.Error(e)
					panic(e)
				}
				log.Info("" + p.ID + " is NOT fetched from Cache")
				pos.Cache()
			} else if er != nil {
				log.Error(er)
				panic(er)
			} else if !bool(v.Available) {
				service.InvalidateCache(v.ID)
			} else {
				log.Info("" + v.ID + " is fetched from Cache")
			}
		}
	}
}
