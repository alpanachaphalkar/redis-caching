package main

import (
	"encoding/json"
	"net/http"
	"strconv"

	log "github.com/sirupsen/logrus"
	svc "github.com/xPlorinRolyPoly/redis-caching/taxis/services"
)

func main() {
	log.Info("Taxis Service")
	handleRequests()
}

func handleRequests() {
	http.HandleFunc("/taxis", returnNearestTaxis)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func returnNearestTaxis(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	log.Info("Endpoint Hit: returnNearestTaxis")
	xS := r.URL.Query().Get("x")
	yS := r.URL.Query().Get("y")
	x, e := strconv.Atoi(xS)
	if e != nil {
		panic(e)
	}
	y, er := strconv.Atoi(yS)
	if er != nil {
		panic(er)
	}
	resp := getNearestTeaxis(x, y)
	if len(resp) == 0 {
		w.Write([]byte("[]"))
	} else {
		json.NewEncoder(w).Encode(resp)
	}
}

func getNearestTeaxis(x, y int) []svc.Position {
	ps := svc.GetCache()
	var respP []svc.Position
	for _, p := range ps {
		r := svc.GetRadius(x, y, p.PosX, p.PosY)
		if r <= 10 {
			log.Printf("Radius: %v", r)
			respP = append(respP, p)
		}
	}
	log.Printf("Nearest taxis: %v", respP)
	return respP
}
