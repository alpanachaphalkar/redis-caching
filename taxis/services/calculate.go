package services

import (
	"math"
)

func GetRadius(x , y , px, py int) (int){
	dx := math.Abs(float64(px)-float64(x))
	dy := math.Abs(float64(py)-float64(y))
	r2 := (dx * dx) + (dy * dy)
	r := math.Sqrt(r2)
	radius := int(math.Round(r))
	return radius
}