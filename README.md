# Simple Redis Caching

## Functional Flow
![Functional Flow](docs/functional-flow.png "Functional Flow")

## REST API: /positions
[Here](positions/README.md) is the documentation.

## Ingestor Service
[Here](ingestor/README.md) is the documentation.

## Caching System
Redis Database is used as a Caching System.
- Add Taxi Positions with 3 minutes of expiration
    ```s
    setex taxi-3 180 "{"ID": "taxi-3", "PosX": 63, "PosY":81, "Available":true}"
    ```
- See if expired
    ```s
    get taxi-3
    ```
    returns nil
- Invalidate the cache
    ```s
    del taxi-3
    ```
    
To run redis database locally, run below command:
```s
docker run --name redis -p 6379:6379 -d redis
```

## REST API: /taxis
[Here](taxis/README.md) is the documentation.

## Requirements
- Installation of [docker desktop](https://www.docker.com/products/docker-desktop)
- Installation of [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- Installation of [k3d](https://github.com/rancher/k3d#get)

## Deployment Setup

### k3s cluster provisioning
Use following command to create the cluster with loadbalancer
```s
k3d cluster create k3s-cluster --agents 4 --port 80:80@loadbalancer
```

Use following command to delete the cluster
```s
k3d cluster delete k3s-cluster
```

### Argo CD
```
helm repo add argo https://argoproj.github.io/argo-helm
```

```
kubectl create namespace argo
```

```
helm install -f argo-cd/override.yaml argo-cd argo/argo-cd --namespace argo
```

To route the argo cd ui to argocd.local, use following command:
```s
echo "127.0.0.1 argocd.local" | sudo tee -a /etc/hosts
```

To access the argo cd ui, use following URL:
```
http://argocd.local/argo
```

### Simple Redis Caching App Deployment
Use following command to deploy the simple-cache app
```s
kubectl apply -f argo-cd/applications/simple-cache.yaml
```

To route the simple-cache to api.local, use following command:
```s
echo "127.0.0.1 api.local" | sudo tee -a /etc/hosts
```

To access the simple-cache REST API, use following URL:
```s
http://api.local/taxis
```

Following is the screenshot of simple-cache REST API after the deployment:
![Simple Cache API](docs/simple-cache-api.png "Simple Cache API")