# REST API: /taxis
- Provides a /taxis endpoint that can be queried, providing the user's
  coordinates.
- Returns a list of taxis, whose current locations are in a 10 unit radius around the user.

[Here](Dockerfile) is the Dockerfile to build the docker image.

To run this service locally, use below command:
```s
docker run -e REDIS_HOST=redis -e REDIS_PORT=6379 \
 alpanachaphalkar/taxis:latest
```