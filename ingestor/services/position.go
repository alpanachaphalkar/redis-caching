package services

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	log "github.com/sirupsen/logrus"
	utils "github.com/xPlorinRolyPoly/redis-caching/ingestor/services/commons"
)

type position struct {
	ID        string
	PosX      int
	PosY      int
	Available bool
}
type positions []position

func GetPositions() (p positions) {
	var positionsHost string = utils.GetEnv("POSITIONS_HOST", "localhost")
	var positionsPort string = utils.GetEnv("POSITIONS_PORT", "8080")
	var positionsURL string = "http://" + positionsHost + ":" + positionsPort + "/positions"
	response, err := http.Get(positionsURL)
	if err != nil {
		log.Error(err)
		panic(err)
	}
	defer response.Body.Close()
	contents, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Error(err)
		panic(err)
	}
	json.Unmarshal(contents, &p)
	return
}

func GetPosition(key string) (p position, e error) {
	var positions []position
	positions = GetPositions()
	for _, pos := range positions {
		if pos.ID == key {
			return pos, nil
		}
	}
	return p, errors.New("Taxi with ID '" + key + "' NOT FOUND")
}
