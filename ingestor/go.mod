module github.com/xPlorinRolyPoly/redis-caching/ingestor

go 1.15

require (
	github.com/go-redis/redis/v8 v8.4.10
	github.com/sirupsen/logrus v1.7.0
)
