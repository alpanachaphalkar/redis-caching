# REST API: /positions
[positionserver](positionsserver) runs at port 8080. It provides the information about taxis, in the following format:
```json
[
{
    "ID": <string>,
    "PosX": <int>,
    "PosY": <int>,
    "Available": <boolean>
}, ...
]
```
The above information can be interpreted as follows:
- ID: unique identifier of the taxi of the form `taxi- <number>` (string).
- PosX: horizontal coordinate of the taxi in the grid (integer).
- PosY: vertical coordinate of the taxi in the grid (integer).
- Available: false if the taxi is already taken, true otherwise (boolean).

[Here](Dockerfile) is the Dockerfile to build the docker image.

To run this service locally, use below command:
```s
docker run -p 8080:8080 alpanachaphalkar/positions
```